<?php

/**
 * @file
 * uw_feed_webresources_news.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_feed_webresources_news_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'web_resources_news_feed_block';
  $context->description = 'Display news from web resources website';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'admin/dashboard' => 'admin/dashboard',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-77c9b547eff870bda48703e09ea4baff' => array(
          'module' => 'views',
          'delta' => '77c9b547eff870bda48703e09ea4baff',
          'region' => 'dashboard_main',
          'weight' => '-34',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Display news from web resources website');
  $export['web_resources_news_feed_block'] = $context;

  return $export;
}
