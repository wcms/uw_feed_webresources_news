<?php

/**
 * @file
 * uw_feed_webresources_news.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_feed_webresources_news_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_feed_webresources_news_views_api() {
  return array("api" => "3.0");
}
